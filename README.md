# Bitbucket Browse Code Plugin

A simple plugin for adding a "Browse Code" link on the commits page.

Version 1.0.0 written by Charles O'Farrell, https://bitbucket.org/cofarrell/stash-browse-code-plugin
Version 2.0.0 forked and updated to work with the latest Bitbucket Servers by Marc Teufel, hama GmbH & Co KG

![screenshot](https://bytebucket.org/teufelm/bitbucket-browse-code-plugin/raw/090ae74f3ed31840a187cf82894b03fbb186c6b1/screenshot.png)